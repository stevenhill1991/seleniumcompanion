var webdriver = require('selenium-webdriver');
var driver = require('../framework/index.js')();

var config = require('../framework/config.json');
var Login = require('../framework/login/index.js');

var loginPage = new Login(driver, webdriver);

driver.get(config.url);
loginPage.loginUser();
