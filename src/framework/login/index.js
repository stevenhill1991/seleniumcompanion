var Login = function(driver, webdriver) {
  this.driver = driver;
  this.webdriver = webdriver;
  this.page = require('./loginPage.json');
  this.config = require('../config.json');
};
Login.prototype.getPage = function(){
  return this.driver.get(this.page.url);
};

Login.prototype.waitForPageLoad = function() {
  var self = this;
  return this.driver.wait(function() {
    return self.webdriver.until.ableToSwitchToFrame(self.driver.findElement(self.page.loginIframe.selector))
  }, this.config.driver.defaultTimeout);
};

Login.prototype.switchToIframe = function() {
  return this.driver.switchTo().frame(this.driver.findElement(this.webdriver.By.xpath(this.page.loginIframe.selector.xpath)));
};

Login.prototype.waitForIframeLoad = function() {
  var self = this;

  var deferred = this.webdriver.promise.defer();
  this.driver.wait(function waitForInputField() {
    //wait for the element to be present
    return self.driver.isElementPresent(self.page.usernameInputField.selector);
  }).then(function waitForInputFieldVisible() {
    // then check if it is visible
    return self.driver.wait(self.webdriver.until.elementIsVisible(self.driver.findElement(self.page.usernameInputField.selector)));
  }).then(deferred.fulfill);

  return deferred.promise;
};

Login.prototype.setUserName = function(userName) {
  return this.driver.findElement(this.page.usernameInputField.selector).sendKeys(userName);
};

Login.prototype.setPassword = function(password) {
  return this.driver.findElement(this.page.passwordInputField.selector).sendKeys(password);
};

Login.prototype.submitLoginForm = function() {
  return this.driver.findElement(this.page.submitButtonLogin.selector).click();
};

Login.prototype.loginUser = function() {
  var self = this;
  this.waitForPageLoad().then(function pageLoaded() {
    self.switchToIframe().then(function switchedToIframe() {
      self.waitForIframeLoad().then(function iframeLoaded() {
        self.setUserName(self.config.user.userName);
        self.setPassword(self.config.user.password);
        self.submitLoginForm();
      });
    });
  });
};

Login.prototype.setAuthenticationToken = function(authenticationToken) {
  return this.driver.findElement(this.page.authenticationInputField.selector)
    .sendKeys(authenticationToken);
};

Login.prototype.generateAuthenticationToken = function(authenticationSecret) {
  return totpHelper.generate(authenticationSecret);
};

Login.prototype.setAuthenticationCheckbox = function() {
  return this.driver.findElement(this.page.authenticationCheckbox.selector)
    .click();
};

Login.prototype.submitAuthentication = function() {
  return this.driver.findElement(this.page.authenticationButtonSubmit.selector)
    .click();
};

Login.prototype.authenticateUser = function() {
  return this.driver.wait(function waitForAuthenticationModal() {
    return this.driver.findElement(this.page.authenticationModal.selector)
      .isDisplayed();
  }, this.config.driver.serverRequestTimeout);
  this.setAuthenticationToken(this.config.authenticationSecret);
  this.setAuthenticationCheckbox();
  this.submitAuthentication();
};

module.exports = Login;
