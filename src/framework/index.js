var webdriver = require('selenium-webdriver');

var config = require('./config.json');

function getInstance(){
  var chromeCapabilities = webdriver.Capabilities.chrome();
  chromeCapabilities.set('chromeOptions', config.browser.chrome.chromeOptions);

  return new webdriver.Builder()
    .forBrowser(config.browser.chrome.browserName)
    .withCapabilities(chromeCapabilities)
    .build();
}

module.exports = getInstance;
