var cluster = require('cluster'),
	serverPort = 8080;

if(cluster.isMaster) {

	var cpuCount = require('os').cpus().length;

	for (var i = 0; i < cpuCount; i++) {
		initWorker();
	}

	cluster.on('online', function(worker) {
		console.log('Worker %d is online', worker.id);
	});

	cluster.on('exit', function(worker) {
		console.log('Worker %d died!', worker.id);
		console.log('Starting a new worker');
		initWorker();
	});

	function initWorker() {
		var worker = cluster.fork();

		worker.on('message', function(msg) {
			console.log('Master ' + process.pid + ': received message from worker ' + worker.id + '.', msg);
			handleWorkerMessage(msg, worker.id);
		});
	}

	function handleWorkerMessage(msg, workerId) {
		switch(msg.type) {
			case 'server:restart':
				console.log('Master ' + process.pid + ': restarting ALL workers');
				restartWorkers();
				break;
			case 'server:killWorker':
				console.log('Master ' + process.pid + ': killing WORKER ' + workerId);
				killWorker(workerId);
				break;
			default:
				console.log('Master ' + process.pid + ': UNKNOWN MESSAGE WORKER MESSAGE!');
				break;
		}
	}

	function restartWorkers() {
		var wid, workerIds = [];

		for(wid in cluster.workers) {
			workerIds.push(wid);
		}

		workerIds.forEach(function(wid) {
			if(cluster.workers[wid]){
				cluster.workers[wid].kill();
			}
		});
	}

	function killWorker(workerId) {
		if(cluster.workers[workerId]){
			cluster.workers[workerId].kill();
		}
	}

}

if(cluster.isWorker) {

	var express = require('express');
	var server = express();

	server.get('/', function(req, res) {
		res.send('Hello World from worker ' + cluster.worker.id);
	});

	server.get('/kill', function(req, res) {
		res.send('Worker ' + cluster.worker.id + ' is going to be killed!');
		process.send({
			type: 'server:killWorker'
		})
	});

	server.get('/restart', function(req, res) {
		res.send('Sending restart request to master!');
		process.send({
			type: 'server:restart'
		});
	});

	server.listen(serverPort, function() {
		console.log('Server started on worker %d', cluster.worker.id);
	});

}
